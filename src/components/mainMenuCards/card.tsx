import React from 'react';
import { Link } from 'react-router-dom'
import LinkStyle, { Highlight, H1 } from "@main/components/mainMenuCards/style";

const Card = ({ name, backgroundImageUrl, backgroundHoverImageUrl, icon, linkUrl, ...id }) => {
  return (
    <Link to={ linkUrl } style={ LinkStyle }>
      <div className="card">
        <Highlight backgroundImageUrl={ backgroundImageUrl } backgroundHoverImageUrl={ backgroundHoverImageUrl }>
            <H1 className="card-title">{ name }</H1>
        </Highlight>
      </div>
    </Link>
  )
}

export default Card