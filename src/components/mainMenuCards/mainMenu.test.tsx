import {expect, describe, it} from '@jest/globals';
import React from "react";
import {mount, configure} from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import Card from "@main/components/mainMenuCards/card";
import { BrowserRouter } from 'react-router-dom';
import {routes} from "@main/__data__/constants/routes";


configure({adapter: new Adapter()});

const cards = [
  {
    name: 'My profile',
    backgroundImageUrl: "@main/assets/icons/my-profile.png",
    backgroundHoverImageUrl: "@main/assets/icons/my-profile-click.png",
    icon: 'icon',
    link: routes.homepage + 'profile',
    id: 1
  }
]

describe('Main menu tests', () => {
  it('render my profile', () => {
    const component = mount(
      <BrowserRouter>
        <Card
          name={cards[0].name}
          icon={cards[0].icon}
          backgroundImageUrl={cards[0].backgroundImageUrl}
          backgroundHoverImageUrl={cards[0].backgroundHoverImageUrl}
          linkUrl={cards[0].link}
          key={cards[0].id}
        />
      </BrowserRouter>

    );

    expect(component).toMatchSnapshot()
  })
})