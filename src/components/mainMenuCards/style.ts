import styled from 'styled-components'
import React from "react";

export const Highlight = styled.div`
    height: 30rem;
    width: 30rem; 
    text-align: center;
    display: flex;
    justify-content: center;
    align-items: flex-end;
    background-image: url(${props => props.backgroundImageUrl});
    background-position: center;
    background-size: 20rem;
    background-repeat: no-repeat;  
    background-color: #EEEEEE;
    :hover{
      color: #040C13;
      background: #F75260;
      background-image: url(${props => props.backgroundHoverImageUrl});
      background-position: center;
      background-size: 20rem;
      background-repeat: no-repeat;  
      cursor: pointer; 
    }
`

export const H1 = styled.h1`
    font-family: Roboto;
    font-weight: bold;
    font-size: 50px;
`

const LinkStyle = {
  textDecoration: "none",
  color: "#F75260",
} as React.CSSProperties;

export default LinkStyle