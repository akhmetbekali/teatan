import React from 'react';
import {Link} from 'react-router-dom'
import {Table} from "antd";
import {TeamCardStyle} from "@main/components/team/style";

const mapping = {
  'Marat': '5f651b9727c6ba04bc996ad8',
  'Denis': '5f651fecaf9b030017504cc4',
  'Ali': '5f65206aaf9b030017504cc6',
  'Enes': '5f65b006a3fe7a0017609a0e',
}

const columns = [
  {
    title: 'Firstname',
    dataIndex: 'firstName',
    key: 'firstName',
    width: '400px',
    render: text => <a href={'profile/' + mapping[text]}>{text}</a>
  },
  {
    title: 'Lastname',
    dataIndex: 'lastName',
    key: 'lastName',
    width: '400px'
  },
  {
    title: 'Position',
    dataIndex: 'position',
    key: 'position',
    width: '200px'
  },
  {
    title: 'Rating',
    dataIndex: 'overall',
    key: 'overall',
    width: '200px'
  },
]

const Team = ({team}) => {
  return (
    <div className="card" style={ TeamCardStyle }>
      <span style={{display: 'inline-flex', alignItems: 'center'}}>
        <img src={team.logoUrl} style={{maxWidth: '70px', margin: '20px'}}/>
        <strong style={{fontSize: '60px'}}>{team.name}</strong>
      </span>
      <Table columns={columns}
             dataSource={team.players}
             pagination={false}
             style={{
               fontFamily: "Roboto",
               fontWeight: "bold",
               fontSize: "30px",
             }}
      />
    </div>
  )
}

export default Team