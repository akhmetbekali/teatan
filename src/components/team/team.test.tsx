import React from "react";
import Team from "@main/components/team";
import { expect, describe, it } from '@jest/globals';
import { mount, configure } from "enzyme";
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe("Test Team component", () => {
    it("Render Team component", () => {
        const component = mount(
            <Team team={{
                firstName: 'firstName',
                lastName: 'lastName',
                patronymicName: 'patronymicName',
                citizenship: 'citizenship',
                gender: 'gender',
                birthDate: 'birthDate',
                position: 'position',
                overall: 1,
                pac: 1,
                sho: 1,
                pas: 1,
                dri: 1,
                def: 1,
                phy: 1,
            }}/>
        )
        expect(component).toMatchSnapshot()
    })
})