import * as React from 'react'
import { connect } from 'react-redux'
import actions from '@main/__data__/actions/players'
import {FormEvent} from "react"
import { withRouter } from 'react-router'
import { routes } from '@main/__data__/constants/routes'

interface PropTypes {
  submitPlayer: (player: any) => {}
  history: any
}

class AddPlayerForm extends React.Component<PropTypes, any> {

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  state = {
    birthdateType: 'text',
    firstName: '',
    lastName: '',
    patronymicName: '',
    citizenship: '',
    gender: 'Gender',
    birthDate: '',
    position: 'Position',
    overall: '',
    pac: '',
    sho: '',
    pas: '',
    dri: '',
    def: '',
    phy: '',
    photoUrl: '',
  }

  handleChange = (e) => {
    this.setState({[e.target.id]: e.target.value})
  }

  handleSubmit = (event: FormEvent) => {
    this.props.submitPlayer({
      "firstName": this.state.firstName,
      "lastName": this.state.lastName,
      "patronymicName": this.state.patronymicName,
      "citizenship": this.state.citizenship,
      "gender": this.state.gender,
      "birthDate": this.state.birthDate,
      "position": this.state.position,
      "overall": this.state.overall,
      "pac": this.state.pac,
      "sho": this.state.sho,
      "pas": this.state.pas,
      "dri": this.state.dri,
      "def": this.state.def,
      "phy": this.state.phy,
      "photoUrl": this.state.photoUrl
    })
    this.props.history.push(routes.profile)
    event.preventDefault()
  }

  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <h2>Personal Info</h2>
          <div className='form-group form-row'>
            <div className='col'>
              <input
                type='text'
                required
                className='form-control form-control-lg'
                placeholder='Name'
                id='firstName'
                value={this.state.firstName}
                onChange={this.handleChange}
              />
            </div>
            <div className='col'>
              <input
                type='text'
                required
                className='form-control form-control-lg'
                placeholder='Lastname'
                id='lastName'
                value={this.state.lastName}
                onChange={this.handleChange}
              />
            </div>
            <div className='col'>
              <input
                type='text'
                required
                className='form-control form-control-lg'
                placeholder='Patronymic Name'
                id='patronymicName'
                value={this.state.patronymicName}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className='form-group form-row'>
            <div className='col'>
              <input
                type='text'
                required
                className='form-control form-control-lg'
                placeholder='Citizenship'
                id='citizenship'
                value={this.state.citizenship}
                onChange={this.handleChange}
              />
            </div>
            <div className='col'>
              <select
                className='form-control form-control-lg'
                id='gender'
                required
                value={this.state.gender}
                onChange={this.handleChange}
              >
                <option disabled selected>Gender</option>
                <option>Male</option>
                <option>Female</option>
                <option>Other</option>
              </select>
            </div>
            <div className='col'>
              <input
                id='birthDate'
                type={this.state.birthdateType}
                required
                className='form-control form-control-lg'
                placeholder='Birthdate'
                onFocus={() => {this.setState({birthdateType: 'date'})}}
                onBlur={() => {this.setState({birthdateType: 'text'})}}
                value={this.state.birthDate}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className='form-group form-row'>
            <div className='col-6'>
              <label>Photo</label>
              <input
                type='text'
                required
                className='form-control form-control-lg'
                placeholder='Photo URL'
                id='photoUrl'
                value={this.state.photoUrl}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <h2>Playing Stats</h2>
          <div className='form-group form-row'>
            <div className='col'>
              <select
                className='form-control form-control-lg'
                id='position'
                required
                value={this.state.position}
                onChange={this.handleChange}
              >
                <option disabled selected>Position</option>
                <option>GK</option>
                <option>CB</option>
                <option>LB</option>
                <option>RB</option>
                <option>LWB</option>
                <option>RWB</option>
                <option>CDM</option>
                <option>CM</option>
                <option>CAM</option>
                <option>LM</option>
                <option>RM</option>
                <option>LW</option>
                <option>RW</option>
                <option>ST</option>
              </select>
            </div>
            <div className='col'>
              <input
                type='number'
                required
                className='form-control form-control-lg'
                placeholder='Overall Stats'
                id='overall'
                value={this.state.overall}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className='form-group form-row'>
            <div className='col'>
              <label>PAC</label>
              <input
                type='number'
                required
                className='form-control form-control-lg'
                placeholder='0'
                id='pac'
                value={this.state.pac}
                onChange={this.handleChange}
              />
            </div>
            <div className='col'>
              <label>SHO</label>
              <input
                type='number'
                required
                className='form-control form-control-lg'
                placeholder='0'
                id='sho'
                value={this.state.sho}
                onChange={this.handleChange}
              />
            </div>
            <div className='col'>
              <label>PAS</label>
              <input
                type='number'
                required
                className='form-control form-control-lg'
                placeholder='0'
                id='pas'
                value={this.state.pas}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className='form-group form-row'>
            <div className='col'>
              <label>DRI</label>
              <input
                type='number'
                required
                className='form-control form-control-lg'
                placeholder='0'
                id='dri'
                value={this.state.dri}
                onChange={this.handleChange}
              />
            </div>
            <div className='col'>
              <label>DEF</label>
              <input
                type='number'
                required
                className='form-control form-control-lg'
                placeholder='0'
                id='def'
                value={this.state.def}
                onChange={this.handleChange}
              />
            </div>
            <div className='col'>
              <label>PHY</label>
              <input
                type='number'
                required
                className='form-control form-control-lg'
                placeholder='0'
                id='phy'
                value={this.state.phy}
                onChange={this.handleChange}
              />
            </div>
          </div>
          <div className='float-right'>
            <button type='submit' className='btn btn-primary btn-lg'>Submit</button>
          </div>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    submitPlayer(player : any) {
      dispatch(actions.addPlayer(player))
    }
  }
}

export default connect(null, mapDispatchToProps)(withRouter(AddPlayerForm))
