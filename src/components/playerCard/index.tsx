import React from "react";
import {
  Card,
  PlayerImg,
  Overall,
  Pac,
  Sho,
  Pas,
  Dri,
  Def,
  Phy,
  Name,
  Position,
  TeamImg,
} from "./style";

interface Player {
  firstName: string;
  lastName: string;
  patronymicName: string;
  citizenship: string;
  gender: string;
  birthDate: string;
  position: string;
  overall: number;
  pac: number;
  sho: number;
  pas: number;
  dri: number;
  def: number;
  phy: number;
  photoUrl: string;
  teamId: [any];
}

const URL1 =
  "https://cdn.shopify.com/s/files/1/2412/8291/products/S21ShinyGold_1024x.png?v=1597143241";
//const URL2 = "https://pbs.twimg.com/media/CrcRQqNWAAAOgbn.png";
const URL2 = "https://i.ibb.co/CMdD4d9/Marat.png";
const URL3 =
  "https://upload.wikimedia.org/wikipedia/commons/c/c0/Juventus_FC.png";

interface Prop {
  data: Player;
}

const PlayerCard = ({ data }: Prop) => {
  return (
    <Card>
      <img src={URL1} alt="card" height="500" />
      <PlayerImg src={data.photoUrl} alt="player" height="185px" width="185px" />
      <Position>{data.position}</Position>
      <Overall>{data.overall}</Overall>
      {/* <TeamImg
        src={"https://www.pngarts.com/files/4/Shield-PNG-Image.png"}
        alt="team"
        height="50"
      /> */}
      <Name>{data.lastName}</Name>
      <Pac>{data.pac}</Pac>
      <Sho>{data.sho}</Sho>
      <Pas>{data.pas}</Pas>
      <Dri>{data.dri}</Dri>
      <Def>{data.def}</Def>
      <Phy>{data.phy}</Phy>
    </Card>
  );
};

export default PlayerCard;
