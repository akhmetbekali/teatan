import React from "react";
import styled from "styled-components";

export const Card = styled.div`
  display: inline-flex;
  position: relative;
`;

export const PlayerImg = styled.img`
  position: absolute;
  left: 150px;
  top: 81px;
`;

export const Overall = styled.span`
  position: absolute;
  font-size: 55px;
  font-weight: bold;
  left: 78px;
  top: 40px;
`;

export const Position = styled.span`
  position: absolute;
  left: 88px;
  font-size: 30px;
  font-weight: bold;
  top: 115px;
`;

export const TeamImg = styled.img`
  position: absolute;
  left: 94px;
  top: 208px;
`;

export const Name = styled.span`
  position: absolute;
  width: 298px;
  left: 38px;
  top: 266px;
  font-size: 40px;
  font-weight: bold;
  text-align: center;
`;
const BasicScore = styled.span`
  position: absolute;
  font-size: 30px;
  font-weight: bold;
`;

export const Pac = styled(BasicScore)`
  left: 80px;
  top: 322px;
`;

export const Sho = styled(BasicScore)`
  left: 80px;
  top: 357px;
`;

export const Pas = styled(BasicScore)`
  left: 80px;
  top: 392px;
`;

export const Dri = styled(BasicScore)`
  left: 217px;
  top: 322px;
`;

export const Def = styled(BasicScore)`
  left: 217px;
  top: 357px;
`;

export const Phy = styled(BasicScore)`
  left: 217px;
  top: 392px;
`;
