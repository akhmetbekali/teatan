import { expect, describe, it } from '@jest/globals';
import React from "react";
import { mount, configure } from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import PlayerCard from "@main/components/playerCard/index";

configure({ adapter: new Adapter() });

describe('Player Card tests', () => {
    it('render PlayerCard', () => {
        const component = mount(<
          PlayerCard data={
            {
                firstName: 'string',
                lastName: 'string',
                patronymicName: 'string',
                citizenship: 'string',
                gender: 'string',
                birthDate: 'string',
                position: 'string',
                overall: 99,
                pac: 99,
                sho: 0,
                pas: 1,
                dri: 2,
                def: 3,
                phy: 3,
                photoUrl: 'string',
                teamId: ['any']
            }
        } />);
        expect(component).toMatchSnapshot()
    })
})