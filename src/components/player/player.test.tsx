import { expect, describe, it } from '@jest/globals';
import React from "react";
import { mount, configure } from "enzyme";
import Adapter from 'enzyme-adapter-react-16';
import Player from "@main/components/player/index";

configure({ adapter: new Adapter() });

describe('Player tests', () => {
    it('render player', () => {
        const component = mount(<
          Player player={
            {
                birthDate: "16.05.1996",
                citizenship: "Innopolis",
                def: 99,
                dri: 99,
                firstName: "Marat",
                gender: "Male",
                lastName: "Kashaev",
                overall: 99,
                pac: 99,
                pas: 99,
                patronymicName: "Khamzyaevich",
                photoUrl: "https://i.ibb.co/CMdD4d9/Marat.png",
                phy: 99,
                position: "CA",
                sho: 99,
                teamId: null,
                __v: 0,
                _id: "5f651b9727c6ba04bc996ad8"
            }
        } />);
        expect(component).toMatchSnapshot()
    })
})