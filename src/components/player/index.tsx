import React from "react";
import {
  Dd,
  Dl,
  Dt,
  PlayersDeckStyle30,
  PlayersDeckStyle70,
} from "@main/components/player/style";
import PlayerCard from "../playerCard";
import SkillsChart from "@main/components/SkillsChart";
import {Card, Def, Dri, Pac, Pas, Phy, Sho} from "@main/components/playerCard/style";

const Player = ({ player }) => {
  return (
      <div className="card-deck">
        <div>
          <PlayerCard data={player} />
        </div>
        <div className="card" style={PlayersDeckStyle70}>
          <Dl>
            <Dt>First name</Dt>
            <Dd>{player.firstName}</Dd>

            <Dt>Citizenship</Dt>
            <Dd>{player.citizenship}</Dd>

            <Dt>Last name</Dt>
            <Dd>{player.lastName}</Dd>

            <Dt>Birth date</Dt>
            <Dd>{player.birthDate}</Dd>

            <Dt>Patronymic name</Dt>
            <Dd>{player.patronymicName}</Dd>

            <Dt>Gender</Dt>
            <Dd>{player.gender}</Dd>
          </Dl>
        </div>
        <SkillsChart stats={[{
          pac: player.pac,
          sho: player.sho,
          pas: player.pas,
          dri: player.dri,
          def: player.def,
          phy: player.phy
        }]}/>
    </div>
  );
};

export default Player;
