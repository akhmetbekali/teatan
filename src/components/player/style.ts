import React from "react";
import styled from 'styled-components'

export const Dl = styled.dl`
    display: flex;
    flex-flow: wrap;
    text-align: center;
    align-items: flex-start;
`

export const Dt = styled.dt`
    width: 20%;
    font-family: Roboto;
    font-weight: bold;
    font-size: 30px;
`

export const Dd = styled.dd`
    width: 30%;
    margin: 0;
    font-family: Roboto;
    font-size: 30px;
`

export const PlayersDeckStyle30 = {
  display: "flex",
  flex: "1 0 25%",
  maxWidth: "25%",
  flexDirection: "row",
  alignItems: "center",
  padding: "20px",
  height: "40rem",
  width: "40rem",
  backgroundColor: "#EEEEEE",
} as React.CSSProperties;

export const PlayersDeckStyle70 = {
  display: "flex",
  flexDirection: "row",
  padding: "20px",
  height: "40rem",
  width: "60rem",
  backgroundImage: "linear-gradient(180deg, #E5E5E5 50%, #EEEEEE 50%)",
  backgroundRepeat: "repeat",
  backgroundSize: "8rem 20rem"
} as React.CSSProperties;
