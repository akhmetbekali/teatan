import React from "react";
import Tournament from "@main/components/tournament";
import { expect, describe, it } from '@jest/globals';
import { mount, configure } from "enzyme";
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

describe("Test Tournament component", () => {
    it("Render Tournament component", () => {
        const component = mount(
            <Tournament tournament={[{
                draw: 1,
                ga: 1,
                gf: 1,
                logoUrl: "logoUrl",
                lose: 1,
                name: "name",
                place: 1,
                play: 1,
                players: [],
                pts: 1,
                win: 1,
            }]}/>
        )
        expect(component).toMatchSnapshot()
    })
})