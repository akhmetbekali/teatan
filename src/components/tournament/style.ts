import React from "react";

export const TournamentCardStyle = {
    display: "flex",
    padding: "20px",
    height: "40rem",
    width: "80rem",
    backgroundColor: "#EEEEEE",
} as React.CSSProperties;