import React from 'react';
import {Table} from "antd";
import {TournamentCardStyle} from "@main/components/tournament/style";


const columns = [
    {
        title: 'P',
        dataIndex: 'place',
        key: 'place',
        width: '5%'
    },
    {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '65%'
    },
    {
        title: 'P',
        dataIndex: 'play',
        key: 'play',
        width: '5%'
    },
    {
        title: 'W',
        dataIndex: 'win',
        key: 'win',
        width: '5%'
    },
    {
        title: 'D',
        dataIndex: 'draw',
        key: 'draw',
        width: '5%'
    },
    {
        title: 'L',
        dataIndex: 'lose',
        key: 'lose',
        width: '5%'
    },
    {
        title: 'GF',
        dataIndex: 'gf',
        key: 'gf',
        width: '5%'
    },
    {
        title: 'GA',
        dataIndex: 'ga',
        key: 'ga',
        width: '5%'
    },
    {
        title: 'PTS',
        dataIndex: 'pts',
        key: 'pts',
        width: '5%'
    }
]

const Tournament = ({tournament}) => {
    return (
        <div className="card" style={ TournamentCardStyle }>
            <Table columns={columns}
                   dataSource={tournament}
                   pagination={false}
                   style={{
                       fontFamily: "Roboto",
                       fontWeight: "bold",
                       fontSize: "30px",
                   }}
            />
        </div>
    )
}

{/*const Tournament = ({name, backgroundImage, icon, link}) => {
  return (
    <div>
      <h1> {name}</h1>
      <span>
        <ul> {backgroundImage} </ul>
        <ul> {icon} </ul>
        <ul> <a href={link}>{link}</a> </ul>
      </span>
    </div>

  )
}
*/}

export default Tournament