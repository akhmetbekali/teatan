import React from 'react';
import Tournament from "@main/components/tournament";
import teams from "@main/__data__/actions/teams";
import { connect } from 'react-redux'


interface PropTypes {
    getTeamsProp: () => any,
    loading: boolean,
    teams: any
}

class TournamentContainer extends React.Component<PropTypes, any> {

    componentDidMount() {
        this.props.getTeamsProp()
    }

    render () {
        return (
            <div>
                {this.props.loading && <h2>Loading...</h2>}
                {this.props.teams && <Tournament tournament={this.props.teams}/>}
            </div>
        )
    }
}

const mapStateToProps = (state: any) => {
    return {
        teams: state.getTeams.teams,
        loading: state.getTeams.loading
    }
}

const mapDispatchToProps = (dispatch : any) => {
    return {
        getTeamsProp() {
            dispatch(teams.getTeams())
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TournamentContainer)