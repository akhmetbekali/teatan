import React from 'react';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import * as Thunk from 'redux-thunk';
import { expect, describe, it } from "@jest/globals";
import TournamentContainer from "@main/containers/tournamentsContainer/index";
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

configure({adapter: new Adapter()});
const mockStore = configureStore([Thunk.default]);

const team = {
  _id: "5f6520d2af9b030017504cc7",
  players: [],
  name: "Atletico Madrid",
  place: 2,
  play: 20,
  win: 10,
  draw: 0,
  lose: 10,
  gf: 10,
  ga: 10,
  pts: 30,
  logoUrl: "https://upload.wikimedia.org/wikipedia/ru/thumb/c/c1/Atletico_Madrid_logo.svg/1200px-Atletico_Madrid_logo.svg.png",
  __v: 0
}

describe('mount all app', () => {
  let app;

  it('loading is false', async () => {
    app = renderer.create(
      <Provider store={
        mockStore({
          getTeams: {
            loading: false,
            teams: [team]
          }
        })
      }>
        <TournamentContainer/>
      </Provider>
    );
    expect(app.toJSON()).toMatchSnapshot();
  });

  it('loading is true', async () => {
    app = renderer.create(
      <Provider store={
        mockStore({
          getTeams: {
            loading: true,
            teams: [team]
          }
        })
      }>
        <TournamentContainer/>
      </Provider>
    );
    expect(app.toJSON()).toMatchSnapshot();
  });
});

