import * as React from 'react';
import MetaTags from 'react-meta-tags';
import { Switch, Route } from 'react-router'
import { Link } from 'react-router-dom'
import {Wrapper, Header, H1 } from './style';
import containers from "@main/containers";
import { routes } from "@main/__data__/constants/routes";

const App = () => {
  return (
    <Wrapper>
      <MetaTags>
        <title>Футбольный справочник</title>
        <meta name="description" content="Следите за статистикой игроков и команд в чемпионате."/>
        <meta property="og:title" content="Главная"/>
        <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
          crossOrigin="anonymous"/>
      </MetaTags>

      <Link to={ routes.homepage }>
        <Header>
          <H1>Teatan</H1>
        </Header>
      </Link>
      <Switch>
        <Route exact path={routes.homepage} component={containers.CardsContainer}/>
        <Route path={routes.profile} component={containers.PlayersContainer}/>
        <Route path={routes.team} component={containers.TeamsContainer}/>
        <Route path={routes.tournament} component={containers.TournamentsContainer}/>
        <Route path={routes.addPlayer} component={containers.AddPlayerFormContainer}/>
      </Switch>
    </Wrapper>
  );
};

export default App;
