import styled from 'styled-components'
export const Header = styled.header`
  position: absolute;
  left: 50%;
  transform: translate(-50%, 0%); 
  top: 70px;
  text-decoration: none;
  color: #040C13;
`

export const Wrapper = styled.div`
  background: #E5E5E5;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
    
`
export const H1 = styled.h1`
    color: #F75260;
    font-family: Roboto;
    font-weight: bold;
    font-size: 50px;
    :hover{
      color: #040C13;
    }
`