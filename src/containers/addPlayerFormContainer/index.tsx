import * as React from 'react'
import { connect } from 'react-redux'
import AddPlayerForm from '@main/components/addPlayerForm'
import FormContainerStyle from "@main/containers/addPlayerFormContainer/style";

interface PropTypes {
  loading: boolean
}

class AddPlayerFormContainer extends React.Component<PropTypes, any> {
  render() {
    return (
      <div style={FormContainerStyle}>
        {this.props.loading ? <h1>Loading...</h1> : <AddPlayerForm/>}
      </div>
      )
  }
}

const mapStateToProps = (state: any) => {
  return {
    loading: state.getPlayers.loading
  }
}

export default connect(mapStateToProps)(AddPlayerFormContainer)