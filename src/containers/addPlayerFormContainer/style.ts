import React from "react";

const FormContainerStyle = {
  display: "block",
  width: "70%",
  backgroundColor: "white",
  margin: "auto",
  padding: "30px",
  marginTop: "140px"
} as React.CSSProperties;

export default FormContainerStyle