import CardsContainer from '@main/containers/cardsContainer'
import PlayersContainer from '@main/containers/playersContainer'
import TeamsContainer from '@main/containers/teamsContainer'
import TournamentsContainer from '@main/containers/tournamentsContainer'
import AddPlayerFormContainer from '@main/containers/addPlayerFormContainer'

export default {
  CardsContainer,
  PlayersContainer,
  TeamsContainer,
  TournamentsContainer,
  AddPlayerFormContainer
}