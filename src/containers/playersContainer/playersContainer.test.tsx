import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import * as Thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import {expect, describe, it} from "@jest/globals";
import { BrowserRouter as Router } from 'react-router-dom'
import reducer from '@main/__data__/reducers';
import PlayerContainer from "../playersContainer"
import configureStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

configure({ adapter: new Adapter() });
const mockStore = configureStore([Thunk.default]);
const profile = {
  "_id": "5f65be36a3fe7a0017609a0f",
  "firstName": "Andrey",
  "lastName": "Vlasov",
  "patronymicName": "Unknown",
  "citizenship": "Russia",
  "gender": "Male",
  "birthDate": "17.09.1987",
  "position": "ST",
  "overall": 99,
  "pac": 99,
  "sho": 99,
  "pas": 99,
  "dri": 99,
  "def": 99,
  "phy": 99,
  "photoUrl": "https://i.ibb.co/Hqr0Wwf/Andry.png",
  "__v": 0
}


const mockAnsver = async (mock, responses) => {

  await mock.onAny().reply((config) => {
    const [method, url, ...response] = responses.shift();
    if (config.url === url && config.method.toUpperCase() === method) {
      return response;
    }
    return [500, {}];
  });
}


describe('Mount players Container', () => {
  let mock = new MockAdapter(axios);

  let match;
  let component;
  let match_withoutID;

  match = {
      params:
        {
          id: "5f65be36a3fe7a0017609a0f"
        }

  };
  match_withoutID = {
    params:
      {

      }
  };

  it('mount with id', async () => {
    const app = mount(
      <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
        <Router>
          <PlayerContainer match={match}/>
        </Router>
      </Provider>
    );

    const responses = [
      ['GET', '/api/players/5f65be36a3fe7a0017609a0f', 200, {
        "status": {
          "code": 0
        },
        mainData: {
          ...require('../../../stubs/api/profile.json')
        }
      }
      ]
    ]

    await mockAnsver(mock, responses)

    app.update();
    mock.reset();
    expect(app.find('PlayerContainer')).toMatchSnapshot();
  });

  it('mock without id', async () => {
    const app = mount(
      <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
        <Router>
          <PlayerContainer match={match_withoutID}/>
        </Router>
      </Provider>
    );

    const responses = [
      ['GET', '/api/players/5f65be36a3fe7a0017609a0f', 200, {
        "status": {
          "code": 0
        },
        mainData: {
          ...require('../../../stubs/api/profile.json')
        }
      }
      ]
    ]

    await mockAnsver(mock, responses)

    app.update();
    mock.reset();
    expect(app.find('PlayerContainer')).toMatchSnapshot();
  });

  it('loading is false', async () => {
    component = renderer.create(
      <Provider store={
        mockStore({
          getPlayers: {
            loading: false,
            player: [profile],
            match: match
          }
        })
      }>
        <PlayerContainer match={match} />
      </Provider>
    );
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('loading is true', async () => {
    component = renderer.create(
      <Provider store={
        mockStore({
          getPlayers: {
            loading: false,
            player: [profile],
            match: match
          }
        })
      }>
        <PlayerContainer match={match} />
      </Provider>
    );
    expect(component.toJSON()).toMatchSnapshot();
  });
});

