import React from "react";
import Player from "@main/components/player";
import players from "@main/__data__/actions/players";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import {routes} from "@main/__data__/constants/routes";

interface PropTypes {
  id?: string;
  getPlayer: (id) => any;
  loading: boolean;
  player: any;
  match: any;
}

class PlayerContainer extends React.Component<PropTypes, any> {
  componentDidMount() {
    if (!this.props.player || this.props.match.params.id) {
      this.props.getPlayer(this.props.match.params.id);
    }
  }

  render() {
    return (
      <div>
        {this.props.loading && <h2>Loading...</h2>}
        {this.props.player && <Player player={this.props.player} />}
        {!this.props.player && !this.props.loading && !this.props.match.params.id && <Redirect to={routes.addPlayer}/>}
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    player: state.getPlayers.player,
    loading: state.getPlayers.loading,
  };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    getPlayer(id) {
      if (id) dispatch(players.getPlayer(id));
      else dispatch(players.getPlayer("5f651b9727c6ba04bc996ad8"));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerContainer);
