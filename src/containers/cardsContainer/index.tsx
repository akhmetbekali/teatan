import React from 'react';
import Card from '../../components/mainMenuCards/card'
import CardDeckStyle from "@main/containers/cardsContainer/style";
import { cards } from "@main/__data__/constants/mainMenuCards";


const Cards = () => {
  const elements = cards.map((item) => {
    return (
        <Card
          name={ item.name }
          icon={ item.icon }
          backgroundImageUrl={ item.backgroundImageUrl }
          backgroundHoverImageUrl={ item.backgroundHoverImageUrl }
          linkUrl={ item.link }
          key={ item.id }/>
    )
  })

  return (
    <div className="card-deck" style={ CardDeckStyle }>
      {elements}
    </div>
  )
}

export default Cards