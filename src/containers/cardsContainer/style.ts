import React from "react";

const CardDeckStyle = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "center",
  alignItems: "center",
  padding: "20px"
} as React.CSSProperties;

export default CardDeckStyle