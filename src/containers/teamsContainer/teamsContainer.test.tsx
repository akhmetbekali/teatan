import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import * as Thunk from 'redux-thunk';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import {expect, describe, it} from "@jest/globals";
import { BrowserRouter as Router } from 'react-router-dom'
import reducer from '@main/__data__/reducers';
import TeamContainer from "@main/containers/teamsContainer/index";
import configureStore from 'redux-mock-store';
import renderer from 'react-test-renderer';

configure({ adapter: new Adapter() });
const mockStore = configureStore([Thunk.default]);

const team = {
  _id: "5f6520d2af9b030017504cc7",
  players: [],
  name: "Atletico Madrid",
  place: 2,
  play: 20,
  win: 10,
  draw: 0,
  lose: 10,
  gf: 10,
  ga: 10,
  pts: 30,
  logoUrl: "https://upload.wikimedia.org/wikipedia/ru/thumb/c/c1/Atletico_Madrid_logo.svg/1200px-Atletico_Madrid_logo.svg.png",
  __v: 0
}

const mockAnsver = async (mock, responses) => {

  // Match ALL requests
  await mock.onAny().reply((config) => {
    const [method, url, ...response] = responses.shift();
    if (config.url === url && config.method.toUpperCase() === method) {
      return response;
    }
    // Unexpected request, error out
    return [500, {}];
  });
}


describe('mount all app', () => {
  let mock = new MockAdapter(axios);
  let baseProps;
  let component;

  baseProps = {
    match: {
      params:
        {
          getTeam: undefined,
          loading: false,
          team: [undefined],
        }
    }
  }

  it('mount', async () => {
    const app = mount(
      <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
        <Router>
          <TeamContainer {...baseProps}/>
        </Router>
      </Provider>
    );

    const responses = [
      ['GET', '/api/teams', 200, {
        "status": {
          "code": 0
        },
        mainData: {
          ...require('../../../stubs/api/team.json')
        }
      }
      ]
    ]
    await mockAnsver(mock, responses)
    app.update();
    mock.reset();
    expect(app).toMatchSnapshot();
  });


  it('error', async () => {
    const app = mount(
      <Provider store={createStore(reducer, applyMiddleware(Thunk.default))}>
        <Router>
          <TeamContainer {...baseProps}/>
        </Router>
      </Provider>
    );

    const responses = [
      ['GET', '/api/teams', 500, {
        "status": {
          "code": 500
        },
        mainData: {
          ...require('../../../stubs/api/team.json')
        }
      }
      ]
    ]

    await mockAnsver(mock, responses)

    app.update();
    mock.reset();
    expect(app).toMatchSnapshot();
  });


  it('loading is false', async () => {
    component = renderer.create(
      <Provider store={
        mockStore({
          getTeams: {
            loading: false,
            teams: [team]
          }
        })
      }>
        <TeamContainer/>
      </Provider>
    );
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('loading is true', async () => {
    component = renderer.create(
      <Provider store={
        mockStore({
          getTeams: {
            loading: true,
            teams: [team]
          }
        })
      }>
        <TeamContainer/>
      </Provider>
    );
    expect(component.toJSON()).toMatchSnapshot();
  });
});
