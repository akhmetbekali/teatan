import React from 'react';
import { connect } from 'react-redux';
import Team from "@main/components/team";
import teams from "@main/__data__/actions/teams";
import CardDeckStyle from "@main/containers/cardsContainer/style";

interface PropTypes {
  getTeam: () => any,
  loading: boolean,
  team: [any],
}

class TeamContainer extends React.Component<PropTypes, any> {

  componentDidMount() {
    this.props.getTeam()
  }

  render() {
    return (
      <div className="card-deck" style={ CardDeckStyle }>
        {this.props.loading && <h2>Loading...</h2>}
        {this.props.team && <Team team={ this.props.team } />}
      </div>
    )
  }
}

const mapStateToProps = (state: any) => {
  return {
    team: state.getTeams.teams ? state.getTeams.teams[0] : undefined,
    loading: state.getTeams.loading
  }
}

const mapDispatchToProps = (dispatch: any) => {
  return {
    getTeam() {
      dispatch(teams.getTeams())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TeamContainer)