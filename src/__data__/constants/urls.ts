import { getConfig } from '@ijl/cli'

export const header = {"Content-Type": "application/json"}

export const baseUrl = getConfig()["teatan.api.base"]
export const playersUrl = "/players"
export const teamsUrl = "/teams"
export const attachPlayerUrl = "/teams/add-player"
