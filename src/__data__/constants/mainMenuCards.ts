import {routes} from "@main/__data__/constants/routes"
// @ts-ignore
import my_profile from "@main/assets/icons/my-profile.png"
// @ts-ignore
import my_profile_click from "@main/assets/icons/my-profile-click.png"
// @ts-ignore
import my_team from "@main/assets/icons/my-team.png"
// @ts-ignore
import my_team_click from "@main/assets/icons/my-team-click.png"
// @ts-ignore
import league_table from "@main/assets/icons/league-table.png"
// @ts-ignore
import league_table_click from "@main/assets/icons/league-table-click.png"

export const cards = [
  {
    name: 'My profile',
    backgroundImageUrl: my_profile,
    backgroundHoverImageUrl: my_profile_click,
    icon: 'icon',
    link: routes.homepage + 'profile',
    id: 1
  },
  {
    name: 'My team',
    backgroundImageUrl: my_team,
    backgroundHoverImageUrl: my_team_click,
    icon: 'icon 2',
    link: routes.homepage + 'team',
    id: 2
  },
  {
    name: 'Tournament',
    backgroundImageUrl: league_table,
    backgroundHoverImageUrl: league_table_click,
    icon: 'icon 2',
    link: routes.homepage + 'tournament',
    id: 3
  }
]