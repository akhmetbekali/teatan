export const requestStatus = {
  REQUEST: "_REQUEST",
  SUCCESS: "_SUCCESS",
  FAILURE: "_FAILURE"
}

export const players = {
  GET_PLAYER: "GET_PLAYER",
  GET_PLAYERS: "GET_PLAYERS",
  ADD_PLAYER: "ADD_PLAYER"
}

export const teams = {
  GET_TEAMS: "GET_TEAMS"
}