const baseUrl = '/teatan'

export const routes = {
    homepage: `${baseUrl}/`,
    profile: `${baseUrl}/profile/:id?`,
    tournament: `${baseUrl}/tournament/:id?`,
    team: `${baseUrl}/team/:id?`,
    addPlayer: `${baseUrl}/add-player`
}