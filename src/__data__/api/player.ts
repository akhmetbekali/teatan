import axios from 'axios'
import { header, baseUrl, playersUrl } from "@main/__data__/constants/urls";

function getPlayerInfo(id : String) : Promise<any> {
  return axios.get(`${ baseUrl }${ playersUrl }/${ id }`, {headers: header})
}

function addPlayer(player: any) : Promise<any> {
  return axios.post(`${ baseUrl }${ playersUrl }`, player, {headers: header})
}

export default {
  getPlayerInfo,
  addPlayer
}