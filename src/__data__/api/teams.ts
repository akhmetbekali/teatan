import axios from 'axios'
import { header, baseUrl, teamsUrl } from "@main/__data__/constants/urls";

function getTeams(): Promise<any> {
    return axios.get(`${ baseUrl }${ teamsUrl }`, {headers: header})
}

export default {
    getTeams
}