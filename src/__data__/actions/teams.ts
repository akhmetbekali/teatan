import {requestStatus, teams} from "@main/__data__/constants/actions"
import teamsApi from "@main/__data__/api/teams"

function getTeams() {
    return (dispatch : any) => {
        dispatch({type: teams.GET_TEAMS + requestStatus.REQUEST})
        teamsApi.getTeams()
            .then(res => {
                dispatch({
                    type: teams.GET_TEAMS + requestStatus.SUCCESS,
                    payload: res})
            })
            .catch(error => {
                dispatch({
                    type: teams.GET_TEAMS + requestStatus.FAILURE,
                    payload: error
                })
            })
    }
}

export default {
    getTeams
}