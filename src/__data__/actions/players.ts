import { requestStatus, players } from "@main/__data__/constants/actions"
import playerApi from "@main/__data__/api/player"


function getPlayer(id : String) {
  return (dispatch : any) => {
    dispatch({type: players.GET_PLAYER + requestStatus.REQUEST})
    playerApi.getPlayerInfo(id)
      .then(res => {
        dispatch({
          type: players.GET_PLAYER + requestStatus.SUCCESS,
          payload: res
        })
      })
      .catch(error => {
        dispatch({
          type: players.GET_PLAYER + requestStatus.FAILURE,
          payload: error
        })
      })
  }
}

function addPlayer(player: any) {
  return (dispatch: any) => {
    dispatch({type: players.ADD_PLAYER + requestStatus.REQUEST})
    playerApi.addPlayer(player)
      .then(res => {
        dispatch({
          type: players.ADD_PLAYER + requestStatus.SUCCESS,
          payload: res
        })
      })
      .catch(error => {
        dispatch({
          type: players.ADD_PLAYER + requestStatus.FAILURE,
          payload: error
        })
      })
  }
}

export default {
  getPlayer,
  addPlayer
}