import { combineReducers } from 'redux';
// import reducers here
import { getPlayers } from "@main/__data__/reducers/players";
import { getTeams } from "@main/__data__/reducers/teams";

export default combineReducers({
  // insert reducers here
  getPlayers,
  getTeams
});