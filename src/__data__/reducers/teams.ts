import {teams, requestStatus} from "@main/__data__/constants/actions";

const initialState = []

export function getTeams(state = initialState, action : any) {
    switch (action.type) {
        case teams.GET_TEAMS + requestStatus.REQUEST:
            return { loading: true, ...state }
        case teams.GET_TEAMS + requestStatus.SUCCESS:
            return {
                ...state,
                loading: false,
                teams: action.payload.data.data
            }
        default:
            return state
    }
}