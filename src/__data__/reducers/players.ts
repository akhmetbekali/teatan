import { players, requestStatus } from "@main/__data__/constants/actions";

const initialState = {}

export function getPlayers(state = initialState, action : any) {
  switch (action.type) {
    case players.GET_PLAYER + requestStatus.REQUEST:
    case players.ADD_PLAYER + requestStatus.REQUEST:
      return { loading: true, ...state }
    case players.GET_PLAYER + requestStatus.SUCCESS:
    case players.ADD_PLAYER + requestStatus.SUCCESS:
      return {
        ...state,
        loading: false,
        player: action.payload.data.response
      }
    default:
      return state
  }
}
