import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import * as Thunk from 'redux-thunk';

import reducer from '@main/__data__/reducers';
import Container from '@main/containers/mainContainer';

const logger = createLogger()

export default () => (
    <Provider store={createStore(reducer, applyMiddleware(Thunk.default, logger))}>
        <Router>
            <Container/>
        </Router>
    </Provider>
);