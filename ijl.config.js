const pkg = require("./package")

module.exports = {
  "webpackConfig" : {
    "output": {
      "publicPath": `/static/teatan/${pkg.version}/`
    },
    "resolve": {
      "extensions": [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css", ".jsx"]
    },
  },
  "navigations": {
    "repos": "/teatan"
  },
  config: {
    "teatan.api.base": "/api"
  }
}