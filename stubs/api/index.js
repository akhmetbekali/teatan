const router = require('express').Router()

router.get('/players/:id?', (req, res) => {
    let answer = {
        "status": {
            "code": 200
        },
        response: require("./profile.json")
    }
    res.send(answer)
})

router.get('/teams', ((req, res) => {
    let answer = {
        "status": {
            "code": 200
        },
        data: require('./team.json')
    }
    res.send(answer)
}))

router.post('/players', (req, res) => {
    let answer = {
        "status": {
            "code": 200
        },
        response: req.body.data
    }
    res.send(answer)
})

module.exports = router